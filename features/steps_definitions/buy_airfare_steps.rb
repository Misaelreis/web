
Dado("a página inicial da Maxmilhas") do
  visit('')
end

Quando("pequisar passgens com a origem {string} para {string}") do |cidade1, cidade2|
    @search.fill_origin_and_destiny(cidade1, cidade2)
end

Quando("selecionar uma data") do
  @search.fill_dates(@outbound, @inbound)
end

Quando("adicionar mais um passageiro adt") do
  @search.add_passenger_adt
end

Quando("realizar a oesquisa da passagem") do
  @search.click_button_search
end

Então("o site retorna o preço das passagens") do
  sleep 10
end