class Home <SitePrism::Page
    element :origin, :id, "search_form_from"
    element :destiny,:id, "search_form_to"
    element :outbound_date, :id, "search_form_outbound_date"
    element :inbound_date, :id, "search_form_inbound_date"
    element :button_search, :id, "btn_search_flights"
    element :passenger, :id, "passengersWrapper"
    element :select_passenger, :id, "passengers-adults"

    def fill_origin_and_destiny(cidade1, cidade2)
        origin.set(cidade1)
        sleep 2
        origin.send_keys :arrow_down
        origin.send_keys :arrow_down
        origin.send_keys :enter
        destiny.set(cidade2)
        sleep 2
        destiny.send_keys :arrow_down
        destiny.send_keys :enter
    end
    
    def fill_dates(date1, date2)
        outbound_date.set(date1)
        inbound_date.set(date2)
    end

    def add_passenger_adt
        passenger.click
        select_passenger.select(2)
    end

    def click_button_search
        button_search.click
    end
end